// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "hardhat/console.sol";

contract Wallet {

    address owner;
    address public comissionAddress = 0xdD870fA1b7C4700F2BD7f44238821C26f7392148;
    uint public commissionAmount;

    struct allowanceData {
        address owner;
        IERC20 token;
        uint amount;
    }

    mapping(address => uint) public balance;
    mapping(address => mapping (IERC20 => uint256)) tokenBalances;
    mapping(address => mapping (IERC20 => allowanceData)) public allowance;


    constructor (uint _commissionAmount) {
        owner = msg.sender;
        commissionAmount = _commissionAmount;
    }

    receive() external payable {
        require(msg.value > 0, 'Amount has to be greater than zero.');
        balance[msg.sender] += msg.value;
    }

    modifier onlyOwner {
        require(msg.sender == owner, "You are not allowed.");
        _;
    }

    function setCommission(uint _amount) public onlyOwner {
        commissionAmount = _amount;
    }

    function withdraw(uint _amount) public {
        require(_amount <= balance[msg.sender], 'The amount to withdraw is exceeded your balance.');

        balance[msg.sender] -= _amount;
        payable(msg.sender).transfer(_amount-commissionAmount);
        payable(comissionAddress).transfer(commissionAmount);
    }

    function tokenBalance(IERC20 _token) public view returns(uint) {
        return tokenBalances[msg.sender][_token];
    }

    function receiveToken(IERC20 _token, uint _amount) public {
        require(
            _token.allowance(msg.sender, address(this)) >= _amount,
            "Token allowance too low."
        );
        _token.transferFrom(msg.sender, address(this), _amount);
        tokenBalances[msg.sender][_token] += _amount; 
    }

    function withdrawToken(IERC20 _token, uint _amount) public {
        if(allowance[msg.sender][_token].amount > 0) {
            require(
                tokenBalances[allowance[msg.sender][_token].owner][_token] >= _amount,
                "Owner token balance is too low."
            );
            tokenBalances[allowance[msg.sender][_token].owner][_token] -= _amount;
            _token.transfer(msg.sender, _amount);
            allowance[msg.sender][_token].amount -= _amount;
        } else {
            require(
                tokenBalances[msg.sender][_token] >= _amount,
                "Token balance is too low."
            );
            tokenBalances[msg.sender][_token] -= _amount;
            _token.transfer(msg.sender, _amount);
        }
    }

    function withdrawMissedTokens(IERC20 _token, address _to, uint _amount) public onlyOwner {
        require(_token.balanceOf(address(this)) >= _amount, "Balance is lower than withdrawal amount.");
        _token.transfer(_to, _amount);
    }

    function approveToken(IERC20 _token, address _to, uint _amount) public {
        require(
            tokenBalances[msg.sender][_token] >= _amount,
            "Token balance is too low."
        );
        _token.approve(_to, _amount);
        allowance[_to][_token].owner = msg.sender;
        allowance[_to][_token].token = _token;
        allowance[_to][_token].amount = _amount;
    }
}