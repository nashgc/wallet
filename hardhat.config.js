/**
 * @type import('hardhat/config').HardhatUserConfig
 */
const dotenv = require("dotenv");
dotenv.config({ path: __dirname + "/.env" });
require("@nomiclabs/hardhat-waffle");
require("solidity-coverage");

const { ALCHEMY_API_KEY, RINKEBY_API_KEY } = process.env;

if (!ALCHEMY_API_KEY) {
  console.log("Please provide your ALCHEMY_API_KEY in a .env file");
}

if (!RINKEBY_API_KEY) {
  console.log("Please provide your RINKEBY_API_KEY in a .env file");
}

module.exports = {
  solidity: "0.8.4",
  networks: {
    hardhat: {
    },
    rinkeby: {
      url: ALCHEMY_API_KEY == undefined ? `https://eth-rinkeby.alchemyapi.io/v2/${ALCHEMY_API_KEY}` : '',
      accounts: RINKEBY_API_KEY !== undefined ? [RINKEBY_API_KEY] : [],
    },
  },
};
