const { expect } = require("chai");
const { ethers } = require("hardhat");
const { deployContract, MockProvider, solidity } = require("ethereum-waffle");


describe("Wallet", () => {
  beforeEach(async function () {
    commissionAmount = 1000;
    newcommissionAmount = 10;
    initialSupply = 100;
    comissionAddress = "0xdD870fA1b7C4700F2BD7f44238821C26f7392148";

    [owner, acc1, acc2, acc3] = await ethers.getSigners();

    Wallet = await ethers.getContractFactory("Wallet");
    wallet = await Wallet.connect(owner).deploy(commissionAmount);
    ERC20 = await ethers.getContractFactory("BasicToken");
    erc20 = await ERC20.connect(owner).deploy(initialSupply);
  });

  it("Should be deployed", () => {
    expect(wallet.address).to.be.properAddress;
    expect(erc20.address).to.be.properAddress;
  });

  it("Should has commission amount", async () => {
    expect(await wallet.commissionAmount()).to.be.equal(commissionAmount);
  });

  it("Should has commission address", async () => {
    expect(await wallet.comissionAddress()).to.be.equal(comissionAddress);
    expect(await wallet.comissionAddress()).to.be.properAddress;
  });

  it("Should get balance", async () => {
    await expect(() =>
      owner.sendTransaction({ to: wallet.address, value: 10 })
    ).to.changeEtherBalance(owner, -10);
    const balance = await wallet.balance(owner.address);
    expect(balance).to.be.equal(10);
  });

  it("Requires a send currency amount > 0", async () => {
    await expect(
      owner.sendTransaction({
        to: wallet.address,
        value: 0,
      })
    ).to.be.revertedWith("Amount has to be greater than zero.");
  });

  it("Should receive currency", async () => {
    await expect(() =>
      owner.sendTransaction({ to: wallet.address, value: 10 })
    ).to.changeEtherBalance(owner, -10);
  });

  it("Should fail set commission amount", async () => {
    await expect(wallet.connect(acc1).setCommission(10)).to.be.revertedWith(
      "You are not allowed"
    );
  });

  it("Should set commission amount", async () => {
    await wallet.setCommission(newcommissionAmount);
    expect(await wallet.commissionAmount()).to.be.equal(newcommissionAmount);
  });

  it("Should fail withdawal because of exceeded of funds", async () => {
    await expect(wallet.withdraw(10)).to.be.revertedWith(
      "The amount to withdraw is exceeded your balance."
    );
  });

  it('Should withdraw', async () => {
    await expect(() =>
      owner.sendTransaction({ to: wallet.address, value: 10000 })
    ).to.changeEtherBalance(owner, -10000);
    await expect(() => wallet.withdraw(5000)).to.changeEtherBalances(
          [wallet, owner],
          [-5000, 4000]
        );
  });

  it('Should seek at the empty token balance', async () => {
    const balance = await wallet.tokenBalance(erc20.address);
    expect(balance).to.be.equal(0);
  });

  it('Should seek at the positive token balance', async () => {

    const balance = await wallet.tokenBalance(erc20.address);
    expect(balance).to.be.equal(0);
  });

  it('Should faild with "token allowance" error wile receive token', async () => {
    await expect(wallet.connect(owner).receiveToken(erc20.address, 1))
    .to.be.revertedWith(
      "Token allowance too low."
    );
  });

  it('Should receive token', async () => {
    await erc20.connect(owner).approve(wallet.address, 1);
    await wallet.connect(owner).receiveToken(erc20.address, 1);
    const balance = await wallet.tokenBalance(erc20.address);
    expect(balance).to.be.equal(1);
  });

  it('Should faild with low token balance', async () => {
    await erc20.connect(owner).approve(wallet.address, 1);
    await expect(wallet.connect(owner).withdrawToken(erc20.address, 1))
    .to.be.revertedWith(
      "Token balance is too low."
    );
  });

  it('Should withdraw proper amount of token', async () => {
    await erc20.connect(owner).approve(wallet.address, 1);
    await wallet.connect(owner).receiveToken(erc20.address, 1);

    await expect(() => wallet.connect(owner).withdrawToken(erc20.address, 1))
    .to.changeTokenBalances(erc20, [owner, wallet], [1, -1]);
  });

  it('Should faild with low owner token balance', async () => {
    await erc20.connect(owner).approve(wallet.address, 1); 
    await wallet.connect(owner).receiveToken(erc20.address, 1);
    await wallet.connect(owner).approveToken(erc20.address, acc1.address, 1);
    await wallet.connect(owner).withdrawToken(erc20.address, 1);

    await expect(wallet.connect(acc1).withdrawToken(erc20.address, 1))
    .to.be.revertedWith(
      "Owner token balance is too low."
    );
  });

  it('Should withdraw proper amount of token by allowance', async () => {
    await erc20.connect(owner).approve(wallet.address, 1); 
    await wallet.connect(owner).receiveToken(erc20.address, 1);
    await wallet.connect(owner).approveToken(erc20.address, acc1.address, 1);

    await expect(() => wallet.connect(acc1).withdrawToken(erc20.address, 1))
    .to.changeTokenBalances(erc20, [wallet, acc1], [-1, 1]);

    const allowance = await wallet.allowance(acc1.address, erc20.address);
    expect(allowance.amount).to.be.equal(0);
  });

  it('Should faild withdrawMissedTokens by ownership', async () => {
    await erc20.connect(acc1).approve(wallet.address, 1); 
    await expect(wallet.connect(acc1).withdrawMissedTokens(erc20.address, acc1.address, 1))
    .to.be.revertedWith(
      "You are not allowed."
    );
  });

  it('Should faild withdrawMissedTokens with lower balance', async () => {
    await erc20.connect(owner).approve(wallet.address, 1); 
    await expect(wallet.connect(owner).withdrawMissedTokens(erc20.address, owner.address, 1))
    .to.be.revertedWith(
      "Balance is lower than withdrawal amount."
    );
  });

  it('Should withdraw missed token', async () => {
    await erc20.connect(owner).approve(wallet.address, 1); 
    await erc20.connect(owner).transfer(wallet.address, 1);
    await expect(() => wallet.connect(owner).withdrawMissedTokens(erc20.address, owner.address, 1))
    .to.changeTokenBalances(erc20, [wallet, owner], [-1, 1]);
  });

  it('Should faild approveToken with token balance is too low', async () => {
    await expect(wallet.connect(owner).approveToken(erc20.address, acc1.address, 1))
    .to.be.revertedWith(
      "Token balance is too low."
    );
  });

  it('Should approve token', async () => {
    await erc20.connect(owner).approve(wallet.address, 1); 
    await wallet.connect(owner).receiveToken(erc20.address, 1);
    await wallet.connect(owner).approveToken(erc20.address, acc1.address, 1);

    const allowanceERC20 = await erc20.allowance(wallet.address, acc1.address);
    expect(allowanceERC20).to.be.equal(1);

    const allowance = await wallet.allowance(acc1.address, erc20.address);
    expect(allowance.owner).to.be.equal(owner.address);
    expect(allowance.token).to.be.equal(erc20.address);
    expect(allowance.amount).to.be.equal(1);
  });
});
