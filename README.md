# Wallet contract 

![Coverage](https://camo.githubusercontent.com/2c8b15a3902bc15c0d1e6d70bbf7a1f0f248e2df4b430e25517c7543233530fb/68747470733a2f2f696d672e736869656c64732e696f2f62616467652f436f7665726167652d3130302532352d627269676874677265656e2e737667)

Wallet contract which can hold network currency or any ERC20 tokens.

## Installation & tests

Use npm to install all dependencies

```npm
npm install
```

To check tests coverage
```npm
npx hardhat coverage
```

## Configuration
To work with contract in Rinkeby network, you have to provide api keys in .env file in root project directory.

```npm
ALCHEMY_API_KEY='Your Alchemy API key goes here'
RINKEBY_API_KEY='Your Rinkeby API key goes here'
```

Also you can provide custom commission fee to contract constructor
```npm
COMMISSION='Your commission fee goes here'
```

## Deploy

```npm
# Deploy contract to a localhost
npx hardhat --network localhost run scripts/deploy.js

# Deploy contract to the rinkeby
npx hardhat --network rinkeby run scripts/deploy.js
```

## Usage
No any hardhat tasks here yet. Hope will add it later. This time you should try it your own.

## Contributing ^_^
Pull requests are welcome. 
Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)